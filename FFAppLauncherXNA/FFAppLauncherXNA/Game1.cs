using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using System.Runtime.InteropServices;

namespace FFAppLauncherXNA
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        struct GameLaunchInfoStruct
        {
            public string exeLoc, workDir, name;
            public Texture2D tex;
        }

        string mDir = Directory.GetCurrentDirectory();
        SpriteFont mFont;
        SpriteFont mFontLarger;
        System.Diagnostics.Process mP = null;
        string mBrowser;
        string isFullscreen;

        Texture2D logo;
        List<GameLaunchInfoStruct> mG = new List<GameLaunchInfoStruct>();
        int xOffset = 500;
        int yOffset = 100;
        int logoWidth, logoHeight;
        int logoPadding;
        int imageWidth, imageHeight;
        int imagePadding;

        //size of logo relative to size of screen
        float logoWidthRatio = 1 / 8f;
        //padding from right of screen to left edge of logo and right edge of logo to left edge of menu items
        float logoPaddingRatio = 1 / 12f;
        float imageAspectRatio = 2 / 1f; //width to height
        float imagePaddingRatio = 1 / 10f;
        int numberCols = 6;
        int numberRows = 5;


        bool isPressed = false;
        bool isReleased = true;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        //static extern IntPtr SetFocus(HandleRef hWnd);
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("User32.dll")]
        public static extern Int32 SetForegroundWindow(IntPtr hWnd);
        [DllImport("User32.dll")]
        private static extern IntPtr GetForegroundWindow();

        public void readSettings()
        {
            StreamReader sr = new StreamReader(@"config.txt");
            mBrowser = sr.ReadLine();
            isFullscreen = sr.ReadLine();
            sr.Close();
        }
        public Game1()
        {
            readSettings();
            graphics = new GraphicsDeviceManager(this);
            if (isFullscreen == "full")
                graphics.ToggleFullScreen();
            Window.Title = "Fantastic Arcade";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            base.Initialize();
            System.Windows.Forms.Form MyGameForm = (System.Windows.Forms.Form)System.Windows.Forms.Form.FromHandle(Window.Handle);
            MyGameForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.ApplyChanges();
            //MyGameForm.Location = new System.Drawing.Point(-50, -50);
            ShowWindow(Window.Handle, 3);
            this.IsMouseVisible = true;
            
            Window.AllowUserResizing = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            parseText();
            logo = this.Content.Load<Texture2D>("logo_inverted");
            mFont = this.Content.Load<SpriteFont>("SpriteFont1");
            mFontLarger = this.Content.Load<SpriteFont>("SpriteFont2");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected void Delay(int ms)
        {
            int time = Environment.TickCount;

            while(true)
                if(Environment.TickCount - time >= ms) return;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (mP != null)
            {
                if (mP.HasExited)
                {
                    mP = null;
                    System.Console.WriteLine("program has quit");
                    //Delay(1000);
                    SetForegroundWindow(Window.Handle);
                    ShowWindow(Window.Handle, 3);
                }
            }
            
            int width = graphics.GraphicsDevice.Viewport.Width;
            int height = graphics.GraphicsDevice.Viewport.Height;
            logoPadding = (int)(logoPaddingRatio * width);
            logoWidth = (int)(width * logoWidthRatio);
            logoHeight = (int)(logoWidth * logo.Height / (float)logo.Width);

            xOffset = logoPadding * 2 + logoWidth;
            //calculate how much mojo is left
            int avail = width - (logoWidth + logoPadding * 2);
            imagePadding = (int)((imagePaddingRatio) * avail / (float)numberCols);
            imageWidth = (int)((1 - imagePaddingRatio) * avail / (float)numberCols);
            imageHeight = (int)(imageWidth / (float)imageAspectRatio);
            yOffset = (int)((height - (imagePadding + imageHeight) * numberRows) / 2f);


            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            //see if we clicked 
            MouseState mause = Mouse.GetState();
            if (mause.LeftButton == ButtonState.Pressed)
                isPressed = true;

            if (mause.LeftButton == ButtonState.Released && isPressed)
            {
                isPressed = false;
                isReleased = true;
            }


            base.Update(gameTime);
        }

        private void parseText()
        {
            StreamReader sr = new StreamReader(@"files.txt");
            while (!sr.EndOfStream)
            {
                GameLaunchInfoStruct t = new GameLaunchInfoStruct();
                t.name = sr.ReadLine();
                t.workDir = sr.ReadLine();
                if (t.workDir[t.workDir.Length - 1] != '\\')
                    t.workDir += "\\";
                t.exeLoc = sr.ReadLine();
                //t.tex = (Texture)Resources.LoadAssetAtPath("Assets\\Resources\\" + sr.ReadLine(), typeof(Texture));
                try { 
                    t.tex = Texture2D.FromFile(graphics.GraphicsDevice, sr.ReadLine()); 
                }
                catch (Exception e) { t.tex = null; }
                //TODO iff null replace with kitty picture
                mG.Add(t);
            }
            sr.Close();
        }

        Rectangle makeRect(int x, int y, int w, int h)
        {
            return new Rectangle(x - w / 2, y - h / 2, w, h);
        }

        private bool mouseInside(Rectangle aR)
        {
            MouseState mause = Mouse.GetState();
            if (aR.Contains(mause.X, mause.Y))
                return true;
            return false;
        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (GetForegroundWindow() != System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle)
            {
                if (mP == null)
                {
                    
                }
                return;
            }

            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            spriteBatch.Draw(logo, makeRect(logoPadding + logoWidth / 2, graphics.GraphicsDevice.Viewport.Height / 2, logoWidth, logoHeight), Color.White);
            for (int i = 0; i < mG.Count; i++)
            {
                int r = i % 5;
                int c = i / 5;
                Vector2 center = new Vector2((int)(xOffset + imageWidth / 2 + r * (imageWidth + imagePadding * 2)), (int)(yOffset + c * (imageHeight + imagePadding * 2)));
                Rectangle loc = makeRect((int)center.X,(int)center.Y, imageWidth, imageHeight);

                if (mouseInside(loc))
                {
                    
                    Rectangle inflated = new Rectangle(loc.X, loc.Y, loc.Width, loc.Height);
                    inflated.Inflate(loc.Width / 10, loc.Height / 10);
                    if (mG[i].tex == null)
                        spriteBatch.DrawString(mFont, mG[i].name, center, Color.White, 0, mFont.MeasureString(mG[i].name) / 2, 1.1f, SpriteEffects.None, 0);
                    else
                        spriteBatch.Draw(mG[i].tex, inflated, Color.White);
                    spriteBatch.DrawString(mFontLarger, mG[i].name, new Vector2(xOffset + numberRows * (imageWidth + imagePadding * 2) / 2-imageWidth/2, yOffset - imageHeight - 40), Color.White, 0, mFont.MeasureString(mG[i].name) / 2, 1.1f, SpriteEffects.None, 0);
                    if (isReleased)
                    {
                        try
                        {
                            if (mG[i].exeLoc == "WEB")
                            {
                                mP = System.Diagnostics.Process.Start(mBrowser, mG[i].workDir);
                            }
                            else
                            {
                                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                                startInfo.FileName = mG[i].exeLoc;
                                startInfo.UseShellExecute = true;
                                startInfo.WorkingDirectory = mG[i].workDir;
                                mP = System.Diagnostics.Process.Start(startInfo);
                            }
                        }
                        catch
                        {
                            mP = null;
                        }
                    }
                }
                else
                {
                    if (mG[i].tex == null)
                        spriteBatch.DrawString(mFont, mG[i].name, center, Color.White, 0, mFont.MeasureString(mG[i].name) / 2, 1, SpriteEffects.None, 0);
                    else
                        spriteBatch.Draw(mG[i].tex, loc, Color.White);
                }
            }
            spriteBatch.End();
            isReleased = false;
            base.Draw(gameTime);
        }
    }
}
